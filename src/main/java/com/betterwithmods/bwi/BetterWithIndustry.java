package com.betterwithmods.bwi;

import betterwithmods.module.ModuleLoader;
import com.betterwithmods.bwi.modules.machinery.Machinery;
import com.betterwithmods.bwi.modules.world.WorldModule;
import com.betterwithmods.bwi.network.Network;
import com.betterwithmods.bwi.proxy.IProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import org.apache.logging.log4j.Logger;


@SuppressWarnings({"CanBeFinal", "unused", "WeakerAccess"})
@Mod(modid = BetterWithIndustry.MODID, name = BetterWithIndustry.NAME, version = BetterWithIndustry.VERSION, dependencies = "required-after:betterwithmods")
public class BetterWithIndustry {
    public static final String MODID = "betterwithindustry";
    public static final String NAME = "Better With Industry";
    public static final String VERSION = "${VERSION}";

    public static final ModuleLoader MODULE_LOADER = new ModuleLoader() {
        @Override
        public void registerModules() {
            registerModule(Machinery.class);
            registerModule(WorldModule.class);
        }
    };

    public static Logger logger;

    @SidedProxy(modId = MODID, serverSide = "com.betterwithmods.bwi.proxy.ServerProxy", clientSide = "com.betterwithmods.bwi.proxy.ClientProxy")
    public static IProxy proxy;

    @Mod.Instance(MODID)
    public static BetterWithIndustry instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        MODULE_LOADER.setLogger(logger);
        MODULE_LOADER.preInit(event);
        Network.register();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MODULE_LOADER.init(event);
    }


    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        MODULE_LOADER.postInit(event);
    }


    @Mod.EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        MODULE_LOADER.serverStarting(event);
    }

    @Mod.EventHandler
    public void processIMCMessages(FMLInterModComms.IMCEvent event) {}


    @Mod.EventHandler
    public void serverStarted(FMLServerStartedEvent event) { }

    @Mod.EventHandler
    public void serverStopping(FMLServerStoppingEvent event) { }
}
