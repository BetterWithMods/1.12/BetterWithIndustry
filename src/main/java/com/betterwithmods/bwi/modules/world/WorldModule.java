package com.betterwithmods.bwi.modules.world;

import betterwithmods.module.Module;
import betterwithmods.module.ModuleLoader;
import com.betterwithmods.bwi.modules.world.pollution.PollutionRender;
import com.betterwithmods.bwi.modules.world.pollution.WorldPollution;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class WorldModule extends Module {

    public WorldModule(ModuleLoader loader) {
        super(loader);
    }

    @Override
    public void addFeatures() {
        registerFeature(new WorldPollution());

        if(FMLCommonHandler.instance().getSide().isClient()) {
            registerFeature(new PollutionRender());
        }

    }
}
