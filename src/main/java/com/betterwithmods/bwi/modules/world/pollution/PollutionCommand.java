package com.betterwithmods.bwi.modules.world.pollution;

import com.betterwithmods.bwi.api.pollution.Pollution;
import com.betterwithmods.bwi.network.MessagePollution;
import com.betterwithmods.bwi.network.Network;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.command.CommandTreeBase;

public class PollutionCommand extends CommandTreeBase {

    public PollutionCommand() {
        addSubcommand(new Set());
        addSubcommand(new Get());
    }

    public static Pollution getPollution(ICommandSender sender) {
        return WorldPollution.getPollution(sender.getEntityWorld(), sender.getPosition());
    }

    @Override
    public String getName() {
        return "pollution";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/pollution set";
    }

    public class Get extends CommandBase {

        @Override
        public String getName() {
            return "get";
        }

        @Override
        public String getUsage(ICommandSender sender) {
            return null;
        }

        @Override
        public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
            Pollution p = getPollution(sender);
            if (p != null) {
                sender.sendMessage(new TextComponentString(Double.toString(p.getPollution())));
            }
        }
    }

    public class Set extends CommandBase {

        @Override
        public String getName() {
            return "set";
        }

        @Override
        public String getUsage(ICommandSender sender) {
            return "set <pollution>";
        }

        @Override
        public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
            Pollution p = getPollution(sender);
            if (p != null) {
                if (args.length > 0) {
                    double value = Double.parseDouble(args[0]);
                    p.setPollution(value);
                    Network.sendToAllAround(new MessagePollution(new ChunkPos(sender.getPosition()), p.getPollution()), sender.getEntityWorld(), sender.getPosition());
                }
            }
        }
    }
}
