package com.betterwithmods.bwi.proxy;

import com.betterwithmods.bwi.api.pollution.Pollution;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class ClientProxy implements IProxy {

    public void syncPollution(ChunkPos pos, double pollution) {
        World world = Minecraft.getMinecraft().world;
        Chunk chunk = world.getChunkFromChunkCoords(pos.x,pos.z);
        Pollution p = chunk.getCapability(Pollution.CAPABILITY_POLLUTION, null);
        if(p != null) {
            p.setPollution(pollution);
        }
    }
}
