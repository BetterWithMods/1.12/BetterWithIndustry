package com.betterwithmods.bwi.proxy;

import com.betterwithmods.bwi.api.pollution.Pollution;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public interface IProxy {
    default void syncPollution(ChunkPos pos, double pollution) {
    }
}
