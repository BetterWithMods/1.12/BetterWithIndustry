package com.betterwithmods.bwi.network;

import betterwithmods.network.messages.BWMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.util.math.ChunkPos;

public class MessagePollution extends BWMessage {

    private ChunkPos pos;
    private double pollution;

    public MessagePollution(ChunkPos pos, double pollution) {
        this.pos = pos;
        this.pollution = pollution;
    }

    public MessagePollution() {
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        //TODO chunkpos
        pos = new ChunkPos(readData(buf, int.class), readData(buf,int.class));
        pollution = readData(buf, double.class);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        writeData(buf,pos.x);
        writeData(buf,pos.z);
        writeData(buf, pollution);
    }

    public ChunkPos getPos() {
        return pos;
    }

    public double getPollution() {
        return pollution;
    }
}
