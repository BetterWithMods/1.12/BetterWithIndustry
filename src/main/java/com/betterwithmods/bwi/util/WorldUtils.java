package com.betterwithmods.bwi.util;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class WorldUtils {

    public static void forEachTile(World world, Predicate<TileEntity> filter, Consumer<TileEntity> action) {
        synchronized (world.loadedTileEntityList) {
            world.loadedTileEntityList.stream().parallel().filter(filter).forEach(action);
        }
    }


}
