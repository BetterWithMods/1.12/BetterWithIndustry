package com.betterwithmods.bwi.api.pollution;

public interface IPollutant {
    double getPollutionOutput();

    boolean isPolluting();
}
