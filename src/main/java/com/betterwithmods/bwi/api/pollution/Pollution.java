package com.betterwithmods.bwi.api.pollution;

import com.betterwithmods.bwi.network.MessagePollution;
import com.betterwithmods.bwi.network.Network;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class Pollution implements ICapabilitySerializable<NBTTagCompound> {

    @CapabilityInject(Pollution.class)
    public static Capability<Pollution> CAPABILITY_POLLUTION;

    private double pollution;

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CAPABILITY_POLLUTION;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
        if (hasCapability(capability, facing))
            return CAPABILITY_POLLUTION.cast(this);
        return null;
    }

    @Override
    public NBTTagCompound serializeNBT() {
        NBTTagCompound compound = new NBTTagCompound();
        compound.setDouble("pollution", pollution);
        return compound;
    }

    @Override
    public void deserializeNBT(NBTTagCompound nbt) {
        if (nbt.hasKey("pollution")) {
            this.pollution = nbt.getDouble("pollution");
        }
    }

    public double getPollution() {
        return pollution;
    }

    public void setPollution(double pollution) {
        this.pollution = pollution;
    }

    public void addPollution(double add) {
        setPollution(this.getPollution() + add);
    }

    public static class Storage implements Capability.IStorage<Pollution> {

        @Nullable
        @Override
        public NBTBase writeNBT(Capability<Pollution> capability, Pollution instance, EnumFacing side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<Pollution> capability, Pollution instance, EnumFacing side, NBTBase nbt) {
            instance.deserializeNBT((NBTTagCompound) nbt);
        }
    }
}
