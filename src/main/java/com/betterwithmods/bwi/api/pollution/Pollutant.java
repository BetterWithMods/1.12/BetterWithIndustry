package com.betterwithmods.bwi.api.pollution;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

import javax.annotation.Nullable;

public class Pollutant implements IPollutant {

    @CapabilityInject(IPollutant.class)
    public static Capability<IPollutant> CAPABILITY_POLLUTION_SOURCE;

    @Override
    public double getPollutionOutput() {
        return 0;
    }

    @Override
    public boolean isPolluting() {
        return false;
    }

    public static class Storage implements Capability.IStorage<IPollutant> {

        @Nullable
        @Override
        public NBTBase writeNBT(Capability<IPollutant> capability, IPollutant instance, EnumFacing side) {
            return null;
        }

        @Override
        public void readNBT(Capability<IPollutant> capability, IPollutant instance, EnumFacing side, NBTBase nbt) {

        }
    }
}
